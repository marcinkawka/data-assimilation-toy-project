import numpy as np
import sys

def main(argv):
	#printout setup
	np.set_printoptions(formatter={'float': '{: 0.4f}'.format})

	#parameters
	L=1
	N=3
	L_c=0.1*L

	H,p = H_operator(N,4,N)
	print(H)
	print(p)
	#Orthogonal Fourier Matrix
'''	F=generate_F(L,N)
	print("Orthogonal Fourier matrix")
	print(F)
	#test if we get identity matrix
	print("F*FT")
	print(np.dot(F,F.T))

	C=FOAR_specta(L,N,L_c)
	print("C")
	print(C)
	print("Tr(C) = "+str(np.trace(C)))

	print("FC")
	print(np.dot(F,C))
'''

def H_operator(N,inetwork,idx):
	if(inetwork ==1):
		H=np.zeros(2*N+1)
		H[N]=1
		return H,1

	elif(inetwork == 2):
		H=np.zeros([2,2*N+1])
		H[0,N-idx]=1
		H[1,N+idx]=1
		return H,2

	elif(inetwork == 3):
		H=np.zeros([N+1,2*N+1])
		for i in range(N,2*N+1):
			H[i-N,i]=1
		return H,N
	
	elif(inetwork == 4)	:
		H=np.eye(2*N+1)
		return H,2*N+1


def FOAR_specta(L,N,L_c):

	suma_FOAR = 1+2*np.sum([(1+(2*np.pi*n*L_c/L)**2)**(-1) for n in range(1,N+1)])
	C_0FOAR  = (2*N+1)/suma_FOAR
	d=np.zeros(2*N+1)
	
	for i in range(d.shape[0]):
		m=np.ceil(i/2)
		d[i]=C_0FOAR/(1+(2*np.pi*m*L_c/L)**2)

	C_FOAR=np.diag(d)    
	return C_FOAR


def SOAR_specta(L,N,L_c):

	suma_SOAR = 1+2*np.sum([(1+(2*np.pi*n*L_c/L)**2)**(-2) for n in range(1,N+1)])
	C_0SOAR  = (2*N+1)/suma_SOAR

	d=np.zeros(2*N+1)
	for i in range(d.shape[0]):
		m=np.ceil(i/2)
		d[i]=C_0SOAR/(1+(2*np.pi*m*L_c/L)**2)**2

	C_SOAR=np.diag(d)    
	return C_SOAR

def Gauss_specta(L,N,L_c):
	
	suma_Gauss = 1+2*np.sum([(np.exp(-0.5*(2*np.pi*n*L_c/L)**2)) for n in range(1,N+1)])
	C_0Gauss = (2*N+1)/suma_Gauss

	d=np.zeros(2*N+1)
	for i in range(d.shape[0]):
		m=np.ceil(i/2)
		d[i]=C_0Gauss*np.exp(-0.5*(2*np.pi*m*L_c/L)**2)

	C_Gauss=np.diag(d)    
	return C_Gauss


def generate_F(L,N):
	j=np.array([a-N for a in range(0,2*N+1)])
	x=np.array([L*(a)/(2*N+1) for a in j])
	F=np.zeros([2*N+1,2*N+1])
	C_j0=np.array([1/np.sqrt(2) for a in range(0,2*N+1)])


	# saving the 0-th collumn
	F[:,0]=C_j0

	for n in range(1,2*N+1): #collumn by collumn
		if(n % 2 ==1):
			for j in range(x.shape[0]): #row by row
				F[j,n]=np.cos(2*np.pi*np.ceil(n/2)*x[j]/L)
	
		if(n % 2 ==0):
			for j in range(x.shape[0]): #row by row
				F[j,n]=np.sin(2*np.pi*np.ceil(n/2)*x[j]/L)
			
	F=np.sqrt(2/(2*N+1))*F
	return F

if __name__ == "__main__":
	main(sys.argv)